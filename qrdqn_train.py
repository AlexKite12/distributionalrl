import hydra

@hydra.main(config_path="configs", config_name="qrdqn")
def my_app(cfg) -> None:
    print('Initializing logger...', end="")
    logger = hydra.utils.call(cfg.Logger)
    print('\rLogger initialized.             ')

    print('Initializing environment...', end="")
    env = hydra.utils.call(cfg.Environment)
    print('\rEnvironment initialized.             ')

    print('Initializing learner...')
    learner = hydra.utils.instantiate(cfg.Learner,
                                      _recursive_ = False,
                                      configs=cfg)

    print('\rLearner initialized.       ')
    print("Start learning.")
    for step in range(int(cfg.total_steps)):
        learner.train_episode(step)
    # rewards = 0.0
    # episode_steps = 0
    # for step in range(int(cfg.total_steps)):
    #     if step == cfg.random_steps:
    #         print("Start using policy.")

    #     if step < cfg.random_steps:
    #         action = env.action_space.sample()
    #     else:
    #         action = learner(obs)
    #     obs, reward, done, _ = env.step(action)
    #     episode_steps += 1

    #     rewards += reward
    #     if done or episode_steps>cfg.total_steps:
    #         obs = env.reset()
    #         logger.add_episode(step, rewards)
    #         episode_steps = 0
    #         rewards = 0.0

    #     learner.store_step(next_state=obs, reward=reward, done=done, action=action)

    #     if learner.full and step%cfg.update_freq==0:
    #         logger.start_log(step)
    #         loss = learner.learn_step(step)
    #         logger.add_loss(step, loss)
    #         #learner.save(step)

    #     logger.log(step)

if __name__ == "__main__":
    my_app()
