import torch
import torch.nn as nn
import torch.nn.functional as F
from distributional.utils.registry import NetRegistryBase
#from ..utils.registry import NetRegistryBase


def weight_init_kaiming(layer):
    torch.nn.init.kaiming_normal_(layer.weight, nonlinearity='relu')
    return layer

class C51Net(nn.Module, metaclass=NetRegistryBase):
    def __init__(self, 
                 observation_space: int,
                 action_space: int,
                 n_atoms: int = 51,
                 **kwargs
                 ) -> None:
        super(C51Net, self).__init__()
        self.observation_space = observation_space
        self.action_space = action_space
        print()
        self.n_atoms = n_atoms
        self.backbone = nn.Sequential(
                            nn.Conv2d(observation_space,
                                    32, kernel_size=8, stride=4), nn.ReLU(),
                            nn.Conv2d(32, 64, kernel_size=4, stride=2), nn.ReLU(),
                            nn.Conv2d(64, 64, kernel_size=3, stride=1), nn.ReLU()
                            )
        fc1 = weight_init_kaiming(nn.Linear(7 * 7 * 64, 512))
        fc2 = weight_init_kaiming(nn.Linear(512, action_space * n_atoms))
        self.head = nn.Sequential(fc1, nn.ReLU(), fc2)

        # TODO: init back as xavier, head as kaiming

    def forward(self, x) -> torch.Tensor:
        feats = self.backbone(x / 255.)
        feats = feats.view(feats.size(0), -1)
        vals = self.head(feats)
        vals = vals.view(vals.size(0), self.action_space, self.n_atoms)
        #q_vals = F.softmax(vals, dim=2)
        return vals
    
    def save(self, PATH):
        torch.save(self.state_dict(),PATH)

    def load(self, PATH):
        self.load_state_dict(torch.load(PATH))

if __name__ == '__main__':
    B, C, H, W = 5, 3, 84, 84
    A_SPACE = 6
    net = NetRegistryBase.NET_REGISTRY['c51net']
    f = net(C, A_SPACE, 3)
    print(f.head)
    x = torch.randn((B,C,H,W))
    print(f(x))
    print(C51Net.get_model_registry())
    for k in NetRegistryBase.NET_REGISTRY:
        print(k)
