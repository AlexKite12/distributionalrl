from typing import Any


class NetRegistryBase(type):
    NET_REGISTRY={}

    def __new__(cls, name, bases, attrs):
        """
        Set up cls pool
        :param name: class name
        :param bases: class parents
        :param attrs: dict: includes class attributes, class methods,
        static methods, etc
        :return: new class
        """
        #print(cls, name, bases, attrs)
        new_cls = type.__new__(cls, name, bases, attrs)
        cls.NET_REGISTRY[new_cls.__name__.lower()] = new_cls
        return new_cls

    @classmethod
    def get_model_registry(cls):
        return dict(cls.NET_REGISTRY)


