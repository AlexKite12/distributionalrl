import torch


def calculate_huber_loss(td_errors, kappa=1.0):
    return torch.where(
        td_errors.abs() <= kappa,
        0.5 * td_errors.pow(2),
        kappa * (td_errors.abs() - 0.5 * kappa))

def calculate_huber(td_error, tau, weights, kappa):
    element_wise_huber_loss  = calculate_huber_loss(td_error, kappa)
    element_wise_quantile_huber_loss = torch.abs(
        tau[..., None] - (td_error.detach() < 0).float()
        ) * element_wise_huber_loss / kappa
    # Quantile huber loss.
    batch_quantile_huber_loss = element_wise_quantile_huber_loss.sum(
        dim=1).mean(dim=1, keepdim=True)
    if weights is not None:
        quantile_huber_loss = (batch_quantile_huber_loss * weights).mean()
    else:
        quantile_huber_loss = batch_quantile_huber_loss.mean()

    return quantile_huber_loss