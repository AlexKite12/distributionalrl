import numpy as np
import random
import torch

from collections import defaultdict, namedtuple, deque
from typing import Any, Callable, Dict, List, Optional, Type, TypeVar, Tuple, Union


class BufferReplay:
    def __init__(self,
                observation_space: Union[torch.Tensor, np.ndarray],
                maxlen: int = 1000000,
                minibatch_size: int = 32,
                minimal_size = 50000,
                n_step = 3,
                gamma = 0.99,
                device="cpu"
                ) -> None:
        self.minibatch_size = minibatch_size
        self.maxlen = maxlen
        self.minimal_size = minimal_size
        self.n_step = n_step
        self.gamma = gamma
        self.device = device

        self.step = 0
        self.state = torch.zeros(self.maxlen + 1, *observation_space.shape).type(torch.float32)
        self.action = torch.zeros(self.maxlen)
        self.reward = torch.zeros(self.maxlen)
        self.done = torch.zeros(self.maxlen)
        self.full_idx = 0

    def sample_idx(self):
        return torch.randint(len(self), (self.minibatch_size,)), None

    def sample(self):
        idxs, weights = self.sample_idx()
        #### Need to check the case of step in idxs
        sign = 1 if self.step < self.maxlen/2 else -1
        idxs += (1+self.n_step) * (idxs<=self.step) * (idxs>self.step - self.n_step - 1) * sign

        obs = self.state[idxs]
        next_obs = self.state[idxs + self.n_step + 1]
        reward = self.reward[idxs]
        done = self.done[idxs]
        for i in range(self.n_step):
            reward += self.reward[idxs+i]*self.gamma**(i+1)*(1-done)
            done = bool(self.done[idxs+i] + done)
        action = self.action[idxs]
        device = self.device
        if weights is not None:
            weights = weights.to(device)
        return obs.to(device), next_obs.to(device), action.to(device), reward.to(device), done.to(device), idxs, weights

    @property
    def full(self):
        return self.full_idx >= self.minimal_size

    def push(self, next_state=None, reward=None, done=None, action=None):
        self.state[self.step + 1] = next_state
        self.action[self.step] = action
        self.reward[self.step] = reward
        self.done[self.step] = done

        self.full_idx += 1*(self.full_idx < self.maxlen - self.n_step - 1)
        self.step += 1
        if self.step > self.maxlen - 1:
            self.step = 0

    def reset(self, state):
        self.state[0] = state

    def last(self):
        if self.step > self.maxlen - 1:
            self.step = 0
        return self.state[self.step], self.state[self.step], self.action[self.step], self.reward[self.step], self.done[self.step]
    
    def __len__(self):
        return self.full_idx
        
# 
