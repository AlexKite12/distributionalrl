import numpy as np
import random
import torch

from collections import defaultdict, namedtuple, deque
from typing import Any, Callable, Dict, List, Optional, Type, TypeVar, Tuple, Union
from distributional.storage.buffer import BufferReplay


class PriorityDQNBuffer(BufferReplay):
    def __init__(self,
                observation_space: Union[torch.Tensor, np.ndarray],
                action_space: Union[torch.Tensor, np.ndarray],
                maxlen: int = 1000000,
                minibatch_size: int = 32,
                minimal_size = 50000,
                n_step = 3,
                gamma = 0.99,
                omega=0.5, initial_beta=0.4, final_beta=1.0, beta_steps=100000,
                device="cpu"
                ) -> None:
        super().__init__(observation_space, action_space,
                         maxlen=maxlen, minibatch_size=minibatch_size, minimal_size=minimal_size,
                         n_step=n_step, gamma=gamma, device=device)
        self.priorities = torch.zeros(maxlen)
        self.max_priority = 1.
        self.omega = omega
        self.beta = initial_beta
        self.initial_beta = initial_beta
        self.final_beta = final_beta
        self.beta_steps = beta_steps

    def push(self, next_state=None, reward=None, done=None, action=None):
        self.priorities[self.step] = self.max_priority**(self.omega)
        super().push(next_state=next_state, reward=reward, done=done, action=action)


    def sample_idx(self):
        idxs = self.priorities[:-self.n_step-2].multinomial(self.minibatch_size)
        weights = (len(self)*self.priorities[idxs]/self.priorities.sum()).pow(-self.beta)
        weights /= weights.max()
        return idxs, weights

    def update_priority(self, errors, idxs, step):
        errors = errors.detach().cpu()
        if errors.max() > self.max_priority:
            self.max_priority = errors.max()
        self.priorities[idxs] = errors.pow(self.omega)
        self.beta = (self.initial_beta*(self.beta_steps - step)
                    + self.final_beta*step)/self.beta_steps
        if step >= self.beta_steps:
            self.beta = self.final_beta
