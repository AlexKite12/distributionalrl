import torch
import torch.nn.functional as F
import random 
import numpy as np

from hydra.utils import instantiate

log_steps = 10000

class BaseLearner:
    def store_step(self, **kwargs):
        for key in kwargs:
            kwargs[key]  = torch.tensor(kwargs[key], dtype = torch.float)
        self.storage.push(**kwargs)

    def reset(self, obs):
        self.storage.reset(obs)

    @property
    def full(self):
        return self.storage.full

    @property
    def support(self):
        return self.agent.model.support

    @property
    def max_reward(self):
        return self.agent.model.max_reward

    @property
    def min_reward(self):
        return self.agent.model.min_reward

    @property
    def n_atoms(self):
        return self.agent.model.n_atoms

    @property
    def distributional(self):
        return self.n_atoms > 1

    @property
    def gamma(self):
        return self.storage.gamma

    @property
    def delta(self):
        return (self.max_reward - self.min_reward)/(self.n_atoms-1)

    def __call__(self, obs, deterministic=False):
        obs = obs.to(self.device).unsqueeze(0)
        action, best_action = self.agent.step(obs)
        if deterministic:
            action = best_action
        return action.cpu().detach()

    def update_target(self, hard=False):
        tau = 1. if hard else self.tau
        for par, old_par in zip(self.agent.parameters(), self.old_agent.parameters()):
            old_par.data = (1-tau) * old_par.data + tau * par.data