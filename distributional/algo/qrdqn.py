import torch
import torch.nn.functional as F
import random 
import numpy as np

from hydra.utils import instantiate
from hydra.utils import call
from distributional.algo.base import BaseLearner
from distributional.utils.loss import calculate_huber

log_steps = 10000

# TODO: add params:
#           env: environment
#           max_steps: max steps of training
#           gamma: 
#           tau:
#           kappa
class QRDQNLearner(BaseLearner):
    def __init__(self,
                agent, storage, env, configs,
                target_update_freq: int = 10000, tau=0.005,
                lr=6.25e-5, priority_sampling=False,
                device=None
                ):
        self.target_update_freq = target_update_freq
        self.tau = tau
        self.env = call(configs.Environment)
        if device is not None and torch.cuda.is_available():
            self.device = device
        else:
            self.device = 'cpu'
        observation_space = self.env.observation_space
        action_space=self.env.action_space
        self.storage = instantiate(storage,
                                   observation_space=observation_space)
        self.priority_sampling = "Priority" in self.storage.__class__.__name__

        self.agent = instantiate(agent,
                                 _recursive_ = False,
                                 observation_space=observation_space,
                                 action_space=action_space,).to(device)
        self.optim = torch.optim.Adam(self.agent.parameters(), lr=lr, eps=1.5e-4)

        self.old_agent = instantiate(agent,
                                     _recursive_ = False,
                                     observation_space=observation_space,
                                     action_space=action_space).to(device)

        self.update_target(hard=True)
        self.random_steps = configs.random_steps
        self.total_steps = configs.total_steps
        self.update_freq = configs.update_freq
        self.max_steps = configs.max_steps
        self.kappa = 0.5
        self.logger = call(configs.Logger)
        self.logger.start_log(0)

        # self.gamma = 0.99
        print("Priority replay:", self.priority_sampling)
        print("Distributional:", self.distributional)

    def explore(self, state: torch.Tensor)-> int:
        """
        Find action by current policy
        :param state: current state from environment
        """
        action, _ = self.agent.step(state.to(self.device))
        return action.item()

    def train_episode(self, step: int):
        """
        Train whole episode
        :param step: number of episode
        """
        episode_step = 0
        done = False
        rewards = 0.0
        state = self.env.reset()
        while not done and episode_step < self.max_steps:
            if step < self.random_steps:
                action = self.env.action_space.sample()
            else:
                action = self.explore(state.unsqueeze(0))
            next_state, reward, done, _ = self.env.step(action)
            self.storage.push(next_state=next_state, reward=reward, done=done, action=action)
            episode_step += 1
            rewards += reward
            state = next_state
            if self.storage.full and step % self.update_freq:
                self.train_interval(step)
        self.logger.add_episode(step, rewards)
        print(f'Episode: {step:<4}  '
              f'episode steps: {episode_step:<4}  '
              f'return: {rewards:<5.1f}')

    def train_interval(self, step):
        self.agent.update_epsilon(step)
        if step % self.target_update_freq == 0:
            self.learn_step(step)

    def learn_step(self, step):
        batch = self.storage.sample()
        quantile_loss, mean_q, errors = self.losses(batch, step)
        self.optim.zero_grad()
        quantile_loss.backward(retain_graph=False)
        # Clip norms of gradients to stebilize training.
        for net in self.agent.model:
            torch.nn.utils.clip_grad_norm_(net.parameters(), None)
        self.optim.step()

    def losses(self, batch, step):
        state, next_state, action, reward, done, idxs, weights = batch
        # 1. Calculate quantile values of current states and actions at taus.
        actions = action[..., None]
        q_vals = self.agent.eval(state, actions)
        with torch.no_grad():
            # 2. Calculate Q values of next states.
            next_vals = self.old_agent(next_state)
            next_q = next_vals.mean(2)
            # 3. Calculate greedy actions
            next_actions = torch.argmax(next_q, dim=1, keepdim=True)
            # 4. Calculate quantile values of next states and actions at tau_hats.
            next_q = self.old_agent.eval(next_state, next_actions)
            # 5. Calculate target quantile values.
            target = reward[..., None] + (1. - done[..., None]) + self.gamma * next_q
        td_error = target - next_q
        assert td_error.shape == (q_vals.size(0), q.vals.size(1), q_vals.size(1))
        weights = None
        huber_loss = calculate_huber(td_error, self.tau, weights, self.kappa)
        return huber_loss, next_q.detach().mean().item(), \
            td_error.detach().abs().sum(dim=1).mean(dim=1, keepdim=True)



if __name__ == '__main__':
    from distributional.model.c51 import C51Net
    from distributional.storage.buffer import BufferReplay
    from distributional.agent.qrdqn import QRDQNAgent

    agent = QRDQNAgent(C51Net, total_steps=1000000, observation_space=3, action_space=6)
    batch_size = 4
    state = torch.randn((batch_size, 3, 84, 84))
    storage = BufferReplay(state, 6,maxlen=1000)
    learner = QRDQNLearner(agent, storage,5)
    #state = torch.randn((batch_size, 3, 84, 84))
    #steps = [2, 50000, 10000000]
    #for step in steps:
    #    agent.update_epsilon(step)
    #    print("Eps ", agent.epsilon)
    #    action, best_action = agent.step(state)
    #    actions = action[..., None]
    #    print(f'random action: {action}, best: {best_action}')
    #    print(agent.eval(state, actions).shape)