import copy
import random
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.distributions as dist

from hydra.utils import instantiate


class QRDQNAgent(nn.Module):
    def __init__(self, model, total_steps,
                 initial_epsilon=1., final_epsilon=0.1,
                 observation_space=None, action_space=None):
        super().__init__()
        self.model = instantiate(model,
                                 observation_space=observation_space.shape[0],
                                 action_space=action_space.n)
        #self.model = model(observation_space=observation_space, action_space=action_space)

        self.epsilon = initial_epsilon
        self.initial_epsilon = initial_epsilon
        self.final_epsilon = final_epsilon
        self.total_steps = total_steps

    def forward(self, observation):
        return self.model(observation)

    def step(self, observation):
        q_values = self(observation)
        z_theta = q_values.mean(2)
        action, best_action = self.epsilon_greedy(z_theta)
        return action, best_action

    def eval(self, observation, actions):
        values = self(observation)
        action_index = actions[..., None].expand(values.size(0), values.size(2), 1)
        q_values = values.gather(dim=1, index=action_index)
        return q_values

    def update_epsilon(self, step):
        self.epsilon = (self.initial_epsilon*(self.total_steps - step) + self.final_epsilon*step)/self.total_steps
        if step >= self.total_steps:
            self.epsilon = self.final_epsilon

    def epsilon_greedy(self, preds):
        """
        Construct an epilson greedy policy with given threshhold by schedule
        Args:
            eps_threshold(float): current epsilon
        Returns:
            action
        """
        best_action = preds.max(1)[1]
        action = torch.where(torch.rand(best_action.shape, device=best_action.device)>self.epsilon,
                             best_action,
                             torch.randint(preds.shape[-1],
                                           best_action.shape,
                                           device=best_action.device))
        return action, best_action


if __name__ == '__main__':
    from distributional.model.c51 import C51Net
    agent = QRDQNAgent(C51Net, total_steps=1000000, observation_space=3, action_space=6)
    batch_size = 4
    state = torch.randn((batch_size, 3, 84, 84))
    steps = [2, 50000, 10000000]
    for step in steps:
        agent.update_epsilon(step)
        print("Eps ", agent.epsilon)
        action, best_action = agent.step(state)
        actions = action[..., None]
        print(f'random action: {action}, best: {best_action}')
        print(agent.eval(state, actions).shape)